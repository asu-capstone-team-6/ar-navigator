attribute vec4 vPosition;
attribute vec2 screenSize;
attribute vec2 scale;
attribute vec2 translation;

void main() {
    vec4 scaledPosition = vPosition;

    scaledPosition.x *= scale.x;
    scaledPosition.y *= scale.y;

    scaledPosition.x += translation.x;
    scaledPosition.y += translation.y;

    scaledPosition.x /= screenSize.x;
    scaledPosition.y /= screenSize.y;

    gl_Position = scaledPosition;
}