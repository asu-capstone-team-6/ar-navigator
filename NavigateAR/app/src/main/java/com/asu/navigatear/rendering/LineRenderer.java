package com.asu.navigatear.rendering;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.asu.navigatear.Navigator;
import com.asu.navigatear.R;
import com.asu.navigatear.WorldState;
import com.google.ar.core.Pose;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Renders a "line" between two points. In actuality, it's a rectangle in order to give the line
 * thickness and perspective.
 */
public class LineRenderer implements Renderable {
    private static final String TAG = LineRenderer.class.getSimpleName();

    private static final int COORDS_PER_VERTEX = 3;
    private static final int VERTEX_STRIDE = COORDS_PER_VERTEX * 4;
    private static final int BYTES_PER_FLOAT = 4;
    private static final int BYTES_PER_SHORT = 2;

    private static final float LINE_WIDTH = 0.4f;

    /**
     * The coordinates of the line. The actual coordinates are not the supplied ones here, they're
     * just for demonstrative purposes.
     */
    private float coords[] = {
            // Points closer to the first pose
            -0.5f, 0.0f, -1.0f,
            +0.5f, 0.0f, -1.0f,

            // Points father from the second pose
            -0.5f, 0.0f, +1.0f,
            +0.5f, 0.0f, +1.0f,
    };

    /**
     * OpenGL doesn't support drawing rectangles, so we make them out of triangles. The draw order
     * here specifies two triangles that will be drawn. The numbers correspond to vertices defined
     * in coords.
     */
    private static final short DRAW_ORDER[] = {
            0, 2, 1,
            1, 2, 3
    };

    static final float COLOR[] = {0.0f, 0.635f, 1.0f, 0.7f};

    /**
     * The unique identifier representing the shader program to use. The shader program is simple
     * and doesn't have any special effects.
     */
    private int program;

    /**
     * The unique identifier representing the matrix uniform. We use this to pass the transformation
     * matrix to the vertex shader.
     */
    private int mvpMatrixUniformId;

    /**
     * The unique identifier representing the position attribute. We use this to pass position info
     * to the vertex shader.
     */
    private int positionAttributeId;

    /**
     * The unique identifier representing the color attribute. We use this to pass in the color of
     * the line.
     */
    private int colorUniformId;

    /**
     * A buffer to store vertex information to be passed to OpenGL.
     */
    private FloatBuffer vertexBuffer;

    /**
     * A buffer to store draw order information to be passed to OpenGL.
     */
    private ShortBuffer drawOrderBuffer;

    public void createOnGlThread(Context context) {
        // Create a buffer to put coordinates in. We need to start with a raw
        // ByteBuffer first so that we can set the endianness of the buffer.
        // OpenGL requires native order.
        ByteBuffer bb = ByteBuffer.allocateDirect(coords.length * BYTES_PER_FLOAT);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(coords);
        vertexBuffer.position(0);

        // Create a buffer to put draw order in. See above comment for details
        bb = ByteBuffer.allocateDirect(DRAW_ORDER.length * BYTES_PER_SHORT);
        bb.order(ByteOrder.nativeOrder());
        drawOrderBuffer = bb.asShortBuffer();
        drawOrderBuffer.put(DRAW_ORDER);
        drawOrderBuffer.position(0);

        // Set up the shader
        program = GLES20.glCreateProgram();
        int vertexShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER,
                R.raw.line_vertex);
        int fragmentShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER,
                R.raw.line_fragment);
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);
        ShaderUtil.checkGLError(TAG, "After attaching shaders");
        GLES20.glLinkProgram(program);

        ShaderUtil.checkGLError(TAG, "After program linking");
    }

    /**
     * Set the location of the two points the line will be drawn between.
     * @param pose1 The first point
     * @param pose2 The second point
     */
    private void setVertices(Pose pose1, Pose pose2) {
        //Get the coordinates of the poses
        float x1 = pose1.tx();
        float y1 = pose1.ty();
        float z1 = pose1.tz();

        float x2 = pose2.tx();
        float y2 = pose2.ty();
        float z2 = pose2.tz();

        // Calculates the unit vector pointing from point 1 to point 2
        float distance = (float) Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(z2 - z1, 2));
        float forwardXUnitVector = (x2 - x1) / distance;
        float forwardZUnitVector = (z2 - z1) / distance;

        // Calculates the unit vector rotated 90 degrees from forward
        float rightXUnitVector = -forwardZUnitVector;
        float rightZUnitVector = forwardXUnitVector;

        // Calculates the unit vector rotated -90 degrees from forward
        float leftXUnitVector = forwardZUnitVector;
        float leftZUnitVector = -forwardXUnitVector;

        coords[0] = x1 + LINE_WIDTH * leftXUnitVector;
        coords[1] = y1;
        coords[2] = z1 + LINE_WIDTH * leftZUnitVector;

        coords[3] = x1 + LINE_WIDTH * rightXUnitVector;
        coords[4] = y1;
        coords[5] = z1 + LINE_WIDTH * rightZUnitVector;

        coords[6] = x2 + LINE_WIDTH * leftXUnitVector;
        coords[7] = y2;
        coords[8] = z2 + LINE_WIDTH * leftZUnitVector;

        coords[9] = x2 + LINE_WIDTH * rightXUnitVector;
        coords[10] = y2;
        coords[11] = z2 + LINE_WIDTH * rightZUnitVector;

        vertexBuffer.put(coords);
        vertexBuffer.position(0);
    }

    /**
     * Draws the line between the user's feet, destination, and next destination.
     * @param ctx the current Android context
     * @param worldState the current AR state
     */
    public void draw(Context ctx, WorldState worldState) {
        Navigator navigator = Navigator.getInstance(ctx);
        if (worldState.getFloorPlane() == null || !navigator.getCurrentPosition().isPresent()
                || worldState.getCurrDestinationPose() == null) {
            return;
        }

        ShaderUtil.checkGLError(TAG, "Before draw");

        // Update the vertices to render to
        setVertices(worldState.getFootPose(),worldState.getCurrDestinationPose());
        drawSingleLine(worldState);
        if(worldState.getNextDestinationPose() != null) {
            setVertices(worldState.getCurrDestinationPose(), worldState.getNextDestinationPose());
            drawSingleLine(worldState);
        }
    }
    /**
     * Draws a line between two points set in setVerticies.
     * If these verticies are not set then this function will not
     * work well.
     * @param worldState the current AR state
     */
    private void drawSingleLine(WorldState worldState) {
        float[] matrix = new float[16];
        Matrix.setIdentityM(matrix, 0);
        Matrix.multiplyMM(matrix, 0, worldState.getCameraViewMatrix(),
                0, matrix, 0);
        Matrix.multiplyMM(matrix, 0, worldState.getProjectionMatrix(),
                0, matrix, 0);

        // Start using the shader
        GLES20.glUseProgram(program);

        // Pass the position information in
        positionAttributeId = GLES20.glGetAttribLocation(program, "vPosition");
        GLES20.glEnableVertexAttribArray(positionAttributeId);
        GLES20.glVertexAttribPointer(positionAttributeId, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                VERTEX_STRIDE, vertexBuffer);

        // Allow semi-transparency
        GLES20.glDepthMask(false);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        // Set the color
        colorUniformId = GLES20.glGetUniformLocation(program, "vColor");
        GLES20.glUniform4fv(colorUniformId, 1, COLOR, 0);

        // Apply the matrix transformations
        mvpMatrixUniformId = GLES20.glGetUniformLocation(program, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mvpMatrixUniformId, 1, false, matrix, 0);

        // Draw
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, DRAW_ORDER.length,
                GLES20.GL_UNSIGNED_SHORT, drawOrderBuffer);

        GLES20.glDisableVertexAttribArray(positionAttributeId);

        GLES20.glDepthMask(true);
        GLES20.glDisable(GLES20.GL_BLEND);

        ShaderUtil.checkGLError(TAG, "After draw");
    }
}
