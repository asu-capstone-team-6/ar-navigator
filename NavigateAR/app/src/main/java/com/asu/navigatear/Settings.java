package com.asu.navigatear;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Switch switchPoi = (Switch) findViewById(R.id.poiSwitch);
        switchPoi.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("poiEnabled",true));
        switchPoi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("poiEnabled", true);
                    editor.apply();
                    //Toast.makeText(Settings.this, "Checked", Toast.LENGTH_LONG).show();
                } else {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("poiEnabled", false);
                    editor.apply();
                    //Toast.makeText(Settings.this, "Unchecked", Toast.LENGTH_LONG).show();
                }
            }
        });

        Switch switchLoc = (Switch) findViewById(R.id.locSwitch);
        switchLoc.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("Poly",true));
        final Context cont = this;
        switchLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("Poly", true);
                    editor.apply();
                    //Toast.makeText(Settings.this, "At poly", Toast.LENGTH_LONG).show();
                    Navigator.getInstance(cont).switchToCampus("polytechnic");
                } else {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("Poly", false);
                    editor.apply();
                    //Toast.makeText(Settings.this, "Not at Poly", Toast.LENGTH_LONG).show();
                    Navigator.getInstance(cont).switchToCampus("statefarm");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void onBackPressed(){
        Intent intent =  new Intent(Settings.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
