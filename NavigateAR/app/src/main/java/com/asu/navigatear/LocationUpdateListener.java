package com.asu.navigatear;

import com.google.android.gms.maps.model.LatLng;

public abstract class LocationUpdateListener{
    public abstract void onLocationChanged(LatLng position);
}