package com.asu.navigatear.rendering;

import android.content.Context;

import com.asu.navigatear.Navigator;
import com.asu.navigatear.WorldState;
import com.google.ar.core.Pose;

import java.io.IOException;

/**
 * Renders the 3D model associated with the user's current position in AR. It is a small platform
 * with a darker center.
 */
public class UserLocationRenderer implements Renderable {
    private static final String TAG = UserLocationRenderer.class.getSimpleName();

    private ObjectRenderer obj;

    public UserLocationRenderer() {
        obj = new ObjectRenderer();

        obj.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
    }

    public void createOnGlThread(Context context) throws IOException {
        obj.createOnGlThread(context,
                "user-location.obj",
                "user-location.png");
    }

    /**
     * Positions the user location model at the given pose.
     * @param pose the pose to position at
     */
    private void atPose(Pose pose) {
        float[] anchorMatrix = new float[16];
        pose.toMatrix(anchorMatrix, 0);
        obj.updateModelMatrix(anchorMatrix, 0.4f);
    }

    /**
     * Draws the user location model at the position provided by atPose.
     * @param ctx the current Android context
     * @param worldState the current AR state
     */
    public void draw(Context ctx, WorldState worldState) {
        Navigator navigator = Navigator.getInstance(ctx);
        if (worldState.getFloorPlane() == null || !navigator.getCurrentPosition().isPresent()) {
            return;
        }

        atPose(worldState.getFootPose());
        obj.draw(worldState.getCameraViewMatrix(),
                worldState.getProjectionMatrix(),
                worldState.getLightIntensity());
    }
}
