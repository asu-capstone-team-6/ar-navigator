package com.asu.navigatear.rendering;

import android.content.Context;

import com.asu.navigatear.MathUtils;
import com.asu.navigatear.Navigator;
import com.asu.navigatear.SensorHelper;
import com.asu.navigatear.WorldState;
import com.google.ar.core.Frame;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;

import java.io.IOException;

/**
 * Renders the 3D model indicating the destination point in AR, a large platform with a spinning
 * outline.
 */
public class POIRenderer implements Renderable {
    private static final String TAG = POIRenderer.class.getSimpleName();

    /**
     * The amount to rotate the border by every frame. Expressed as a quaternion.
     */
    private static final float[] ROTATION_AMOUNT = {0.0f, 0.0087f, 0.0f, 0.999f};

    private ObjectRenderer destination;
    private ObjectRenderer destinationBorder;
    Session s1;
    private float[] borderRotation;
    SensorHelper shelp;

    public POIRenderer(Session session, SensorHelper s) {
        destination = new ObjectRenderer();
        destinationBorder = new ObjectRenderer();

        destination.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
        destinationBorder.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);

        borderRotation = new float[]{0.0f, 0.0f, 0.0f, 1.0f};
        s1 = session;
        shelp = s;
    }

    public void createOnGlThread(Context context) throws IOException {
        destination.createOnGlThread(context,
                "POIMarker.obj",
                "POIMarker.png");

    }

    /**
     * Positions the destination model at the given pose.
     * @param pose the pose to position at
     */
    private void atPose(Pose pose) {
        float[] anchorMatrix = new float[16];
        pose.toMatrix(anchorMatrix, 0);
        destination.updateModelMatrix(anchorMatrix, 0.5f);

        float[] borderTranslation = new float[3];
        pose.getTranslation(borderTranslation, 0);
        borderRotation = MathUtils.multiplyQuaternions(borderRotation, ROTATION_AMOUNT);
        Pose borderPose = new Pose(borderTranslation, borderRotation);
        borderPose.toMatrix(anchorMatrix, 0);
        destinationBorder.updateModelMatrix(anchorMatrix, 0.5f);
    }

    /**
     * Draws a platform at the destination.
     * @param ctx the current Android context
     * @param worldState the current AR state
     */
    public void draw(Context ctx, WorldState worldState) {
        Navigator navigator = Navigator.getInstance(ctx);
        if (worldState.getFloorPlane() == null || !navigator.getCurrentPosition().isPresent()) {
            return;
        }

        //atPose(worldState.getDestinationPose());


        if(navigator.isFacingPOI(shelp)) {
            float[] cameramtx = worldState.getCameraViewMatrix();
            float[] projectionmtx = worldState.getProjectionMatrix();
            float lightIntensity = worldState.getLightIntensity() * 0.5f;
            float scaleFactor = .1f;
            //Pose mCam = Pose.makeTranslation(0.34f, -0.15f, -1f);
            float[] mat = new float[16];
            Pose airRotation = Pose.makeRotation(240f, 180f, 240f, 180f);
            final Pose air = Pose.makeTranslation(0.15f, 4f, -8.7f);
            final Pose newPose = air.compose(airRotation);
            Frame frame = worldState.getFrame();
            frame.getCamera().getDisplayOrientedPose().compose(newPose).toMatrix(mat, 0);
            destination.updateModelMatrix(mat, scaleFactor);

            destination.draw(cameramtx, projectionmtx, lightIntensity);
        }

    }
}
