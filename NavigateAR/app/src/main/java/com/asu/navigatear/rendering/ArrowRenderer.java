package com.asu.navigatear.rendering;

import android.content.Context;
import android.opengl.GLES20;

import com.asu.navigatear.Navigator;
import com.asu.navigatear.R;
import com.asu.navigatear.WorldState;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import de.javagl.obj.Obj;
import de.javagl.obj.ObjData;
import de.javagl.obj.ObjReader;
import de.javagl.obj.ObjUtils;

/**
 * Implements an on-screen arrow that points the user to where the line is if it's offscreen.
 */
public class ArrowRenderer implements Renderable {
    private static final String TAG = ArrowRenderer.class.getSimpleName();

    private static final int COORDS_PER_VERTEX = 3;
    private static final int BYTES_PER_FLOAT = 4;

    /**
     * The field of view of the user. In other words, how far the bearing has to be before we
     * consider the line "off-screen".
     */
    private final float USER_FOV = 0.5f;

    /**
     * The color of the arrow. The value is NavigateAR blue. Alpha is decided at runtime based on
     * where the arrow is on its path.
     */
    private final float COLOR[] = {0.0f, 0.635f, 1.0f};

    /**
     * The unique identifier representing the shader program to use. The shader program scales to
     * fit the size of the screen and has some scaling and transformation functionality.
     */
    private int program;

    /**
     * The unique identifier representing the indexes of the vertices, as stored in GPU memory.
     */
    private int indexBufferId;

    /**
     * The unique identifier representing the arrow vertices, as stored in GPU memory.
     */
    private int vertexBufferId;

    /**
     * The number of indexes.
     */
    private int indexCount;

    /**
     * A value between 0.0 and 1.0 representing where on the screen the arrow is in its
     * across screen animation.
     */
    private float acrossScreenAnimationPos = 0.0f;

    /**
     * Keeps track of how many frames the path has been off screen for. Used to delay showing the
     * arrow.
     */
    private int framesOffscreen;

    /**
     * The amount of frames the path must be offscreen before showing the arrow.
     */
    private final int MAX_FRAMES_OFFSCREEN_BEFORE_SHOW = 30;

    /**
     * True if the arrow is being shown.
     */
    private boolean showing;

    public void createOnGlThread(Context context) throws IOException {
        InputStream objInputStream = context.getAssets().open("arrow.obj");
        Obj obj = ObjReader.read(objInputStream);
        obj = ObjUtils.convertToRenderable(obj);

        int[] buffers = new int[2];
        GLES20.glGenBuffers(2, buffers, 0);
        indexBufferId = buffers[0];
        vertexBufferId = buffers[1];

        IntBuffer wideIndices = ObjData.getFaceVertexIndices(obj, 3);

        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferId);
        FloatBuffer vertexBuffer = ObjData.getVertices(obj);
        GLES20.glBufferData(
                GLES20.GL_ARRAY_BUFFER,
                vertexBuffer.limit() * BYTES_PER_FLOAT,
                vertexBuffer,
                GLES20.GL_STATIC_DRAW);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        ShaderUtil.checkGLError(TAG, "OBJ buffer load");

        ShortBuffer indices = ByteBuffer.allocateDirect(2 * wideIndices.limit())
                .order(ByteOrder.nativeOrder()).asShortBuffer();
        while (wideIndices.hasRemaining()) {
            indices.put((short) wideIndices.get());
        }
        indices.rewind();

        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
        indexCount = indices.limit();
        GLES20.glBufferData(
                GLES20.GL_ELEMENT_ARRAY_BUFFER,
                2 * indexCount, indices, GLES20.GL_STATIC_DRAW);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

        // Set up the shader
        program = GLES20.glCreateProgram();
        int vertexShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER,
                R.raw.arrow_vertex);
        int fragmentShader = ShaderUtil.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER,
                R.raw.arrow_fragment);
        GLES20.glAttachShader(program, vertexShader);
        GLES20.glAttachShader(program, fragmentShader);
        ShaderUtil.checkGLError(TAG, "After attaching shaders");
        GLES20.glLinkProgram(program);

        ShaderUtil.checkGLError(TAG, "After program linking");
    }

    public void draw(Context ctx, WorldState worldState) {
        // Make sure we're going somewhere before we try drawing the arrow
        Navigator navigator = Navigator.getInstance(ctx);
        if (worldState.getFloorPlane() == null || !navigator.getCurrentPosition().isPresent()
                || worldState.getCurrDestinationPose() == null) {
            return;
        }

        // Check if we should draw the arrow based on where the path is
        if (Math.abs(worldState.getCurrDestinationBearing()) <= USER_FOV) {
            // The path is in view, don't show the arrow
            showing = false;
            framesOffscreen = 0;
            acrossScreenAnimationPos = 0.0f;
            return;
        } else if (!showing) {
            // The path is not in view. Wait for a bit before showing the arrow
            if (framesOffscreen > MAX_FRAMES_OFFSCREEN_BEFORE_SHOW) {
                showing = true;
            } else {
                framesOffscreen++;
                return;
            }
        }

        // Figure out which direction the arrow should point
        float direction = 1.0f;
        if (worldState.getCurrDestinationBearing() < 0.0f) {
            direction = -1.0f;
        }

        // Progress in the animation
        acrossScreenAnimationPos += smoothedSpeed(acrossScreenAnimationPos);
        if (acrossScreenAnimationPos > 1.0f) {
            acrossScreenAnimationPos = 0.0f;
        }

        // Get the window screen size
        IntBuffer viewportInfo = IntBuffer.allocate(4);
        GLES20.glGetIntegerv(GLES20.GL_VIEWPORT, viewportInfo);
        int width = viewportInfo.get(2);
        int height = viewportInfo.get(3);

        // Find the position of the arrow
        float xPos = ((acrossScreenAnimationPos * (width*3.0f)) - ((float) (width * 1.5f))) * direction;
        float yPos = 0.0f;

        // Allow semi-transparency
        GLES20.glDepthMask(false);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc (GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        GLES20.glUseProgram(program);

        // Scale the model
        int scaleAttributeId = GLES20.glGetAttribLocation(program, "scale");
        GLES20.glVertexAttrib2f(scaleAttributeId, 200.f * direction, 200.f);

        // Tell the shader the size of the window
        int screenSizeAttributeId = GLES20.glGetAttribLocation(program, "screenSize");
        GLES20.glVertexAttrib2f(screenSizeAttributeId, width, height);

        // Tell the shader the position of the arrow
        int translationAttributeId = GLES20.glGetAttribLocation(program, "translation");
        GLES20.glVertexAttrib2f(translationAttributeId, xPos, yPos);

        // Set the color
        int colorUniformId = GLES20.glGetUniformLocation(program, "vColor");
        float[] color = {
                COLOR[0], COLOR[1], COLOR[2], smoothedAlpha(acrossScreenAnimationPos)
        };
        GLES20.glUniform4fv(colorUniformId, 1, color, 0);

        // Pass the position information in
        int positionAttributeId = GLES20.glGetAttribLocation(program, "vPosition");
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vertexBufferId);
        GLES20.glVertexAttribPointer(
                positionAttributeId,
                COORDS_PER_VERTEX, GLES20.GL_FLOAT,
                false, 0, 0);
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);

        GLES20.glEnableVertexAttribArray(positionAttributeId);

        // Draw
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, indexBufferId);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexCount,
                GLES20.GL_UNSIGNED_SHORT, 0);
        GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

        GLES20.glDisableVertexAttribArray(positionAttributeId);

        GLES20.glUseProgram(0);

        GLES20.glDepthMask(true);
        GLES20.glDisable(GLES20.GL_BLEND);

        ShaderUtil.checkGLError(TAG, "After draw");
    }

    /**
     * Employs an upside-down parabola to give a smooth acceleration and then deceleration of speed
     * during the arrow's path across the screen.
     * @param x the part in the path the arrow is in
     * @return speed
     */
    private float smoothedSpeed(float x) {
        final float SPEED = 1.0f / 60.0f;
        return (-(x*x) + 1.5f) * SPEED;
    }

    /**
     * Employs an upside-down parabola to give a smooth alpha value that is most opaque in the
     * center and fades out on the edges of the arrow's animation.
     * @param x the part in the path the arrow is in
     * @return alpha value
     */
    private float smoothedAlpha(float x) {
        x = Math.abs(0.5f - x);
        x *= 2.0f;
        return (-(x*x) + 1.0f);
    }
}
