package com.asu.navigatear;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kirevikyn on 2/27/2018.
 */

public class Building {
    private String name;
    private String abrev;
    private LatLng location;
    Building(String nm, JSONObject obj) throws JSONException{
        name = obj.getString("name");
        abrev = nm;
        location = new LatLng(obj.getDouble("lat"),obj.getDouble("lng"));
    }
    String getName(){return name;}
    String getAbrev(){return abrev;}
    LatLng getLocation(){return location;}

}
