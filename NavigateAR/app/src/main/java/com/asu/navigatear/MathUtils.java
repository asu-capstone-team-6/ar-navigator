package com.asu.navigatear;

import java.util.List;

/**
 * A utility class providing stateless math operations.
 */
public class MathUtils {

    public static float average(List<Float> numbers) {
        float sum = 0.0f;
        for (Float num : numbers) {
            sum += num;
        }

        return sum / numbers.size();
    }

    public static float stddev(List<Float> numbers) {
        float average = average(numbers);

        float meanSubtractedSum = 0.0f;
        for (Float num : numbers) {
            meanSubtractedSum += Math.pow(num - average, 2);
        }

        return (float) Math.sqrt(meanSubtractedSum / numbers.size());
    }

    public static int maxIndex(List<Float> numbers) {
        int maxI = 0;

        for (int i=0; i<numbers.size(); i++) {
            if (numbers.get(i) > numbers.get(maxI)) {
                maxI = i;
            }
        }

        return maxI;
    }

    public static boolean isAngleBetween(double angle, double border1, double border2) {
        double rAngle = ((border2 - border1) % (2.0*Math.PI) + (2.0*Math.PI)) % (2.0*Math.PI);
        if (rAngle > Math.PI) {
            double temp = border1;
            border1 = border2;
            border2 = temp;
        }

        if (border1 <= border2) {
            return angle >= border1 && angle <= border2;
        } else {
            return angle >= border1 || angle <= border2;
        }
    }

    public static float[] multiplyQuaternions(float[] a, float[] b) {
        return new float[] {
                a[3] * b[0] + a[0] * b[3] + a[1] * b[2] - a[2] * b[1],
                a[3] * b[1] - a[0] * b[2] + a[1] * b[3] + a[2] * b[0],
                a[3] * b[2] + a[0] * b[1] - a[1] * b[0] + a[2] * b[3],
                a[3] * b[3] - a[0] * b[0] - a[1] * b[1] - a[2] * b[2]
        };
    }
}
