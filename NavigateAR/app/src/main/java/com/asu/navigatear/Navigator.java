package com.asu.navigatear;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Collections;


public class Navigator {
    private String TAG = Navigator.class.getSimpleName();

    private final int MIN_UPDATE_TIME = 20; //20 ms or 50Hz
    private final int MIN_UPDATE_DISTANCE = 2; //update every 2 meters moved
    private final double PATH_LOCATION_COMPLETED_THRESHOLD = 8.0; //a point on the tour is completed if you are within this threshold of meters from the actual point.
    private final double RECALCULATE_PATH_THRESHOLD = 15.0; //if a user moved more than 20m from the previous location before another locationupdate is triggered, the path is recalculated.
    /**
     * The user's field of view in radians. Used to decide if the user is facing something or not.
     */
    private final double FIELD_OF_VIEW = Math.toRadians(60.0);
    private final int POINTS_OF_INTEREST_NEAR_ME_RADIUS = 70; //include all points of interest in radius (meters)

    private static Navigator singletonInstance;
    private Context cont;


    private Hashtable<String,Building> buildings;
    private Hashtable<String,Hashtable<String,Building>> buildingsTable;

    private Hashtable<LatLng,ArrayList<LatLng>> edges;
    private Hashtable<String,Hashtable<LatLng,ArrayList<LatLng>>> edgeTable;

    private ArrayList<PointOfInterest> pointsOfInterest;
    private Hashtable<String,ArrayList<PointOfInterest>> pointsOfInterestTable;

    private ArrayList<LatLng> tour;
    private Hashtable<String,ArrayList<LatLng>> tourTable;


    private ArrayList<LatLng> path;
    private boolean tourMode;
    private LatLng destination, currentPosition, previousPosition;
    private LocationManager locMan;
    private String poiName = null;

    private ArrayList<LocationUpdateListener> listeners;
    @SuppressLint("MissingPermission")
    private Navigator(Context cont){
        this.cont = cont;
        listeners = new ArrayList<LocationUpdateListener>();
        tourMode = false;
        loadData();

        boolean atPoly = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("Poly",true);
        String campus = (atPoly)? "polytechnic" : "statefarm";
        switchToCampus(campus);


        locMan = (LocationManager) cont.getSystemService(Context.LOCATION_SERVICE);


        locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_UPDATE_TIME,MIN_UPDATE_DISTANCE,new LocationListener(){
            public void onLocationChanged(Location location) {
                previousPosition = currentPosition;
                currentPosition = new LatLng(location.getLatitude(), location.getLongitude());
                if(path == null || // if path doesn't exist
                        (currentPosition != null && previousPosition != null && distanceBetweenInMeters(currentPosition,previousPosition) > RECALCULATE_PATH_THRESHOLD)){ //or path should be recalculated
                    attemptCalculatePath();
                }
                else{ //path already exists
                    LatLng nextPos = getPointOnPath(0);
                    if(nextPos != null && distanceBetweenInMeters(nextPos, currentPosition) < PATH_LOCATION_COMPLETED_THRESHOLD){
                        Log.w(TAG, "consumed a waypoint because of GPS");
                        toNextPointOnPath();
                    }
                }
                for(LocationUpdateListener l: listeners){
                    l.onLocationChanged(currentPosition);
                }

            }
            public void onStatusChanged(String provider,int status, Bundle extras){}
            public void onProviderDisabled(String provider){}
            public void onProviderEnabled(String provider){}
        });

        Location lastKnown = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(lastKnown != null)
            currentPosition = new LatLng(lastKnown.getLatitude(), lastKnown.getLongitude());
    }


    //singleton getter
    public static synchronized Navigator getInstance(Context c){
        if(singletonInstance == null){
            singletonInstance =  new Navigator(c);
        }else{
            singletonInstance.cont = c;
        }

        return singletonInstance;
    }


    public boolean switchToCampus(String campus){
        if(!edgeTable.containsKey(campus) || !buildingsTable.containsKey(campus) || !pointsOfInterestTable.containsKey(campus) || !tourTable.containsKey(campus)){
            Log.i("failed to load campus","failed to load campus");
            return false;
        }
        edges = edgeTable.get(campus);
        buildings = buildingsTable.get(campus);
        pointsOfInterest = pointsOfInterestTable.get(campus);
        tour = (ArrayList<LatLng>)tourTable.get(campus).clone();
        path = null;
        return true;
    }

    /**
     * Attempts to calculates path
     * (unless it doesn't have sufficient data in which case it doesn't)
     * (see first 'if' regarding 'sufficient data')
     */
    private void attemptCalculatePath(){
        if(currentPosition == null || (!tourMode && destination == null))
            return;
        if(tourMode) {
            path = astar(currentPosition, tour.get(0)); //get a path to the start of the tour
            if(path != null) {
                path.remove(path.size() - 1);
                for (int i = 0; i < tour.size(); i++) { //apend the tour to the end of the existing path
                    path.add(tour.get(i));
                }
            }
        }else{
            path = astar(currentPosition,destination);
        }

    }


    /**
     * Finds the distance between two GPS coordinates as the crow flies.
     * @param point1 point 1
     * @param point2 point 2
     * @return distance in meters
     */
    public static double distanceBetweenInMeters(LatLng point1, LatLng point2) {
        // Implements the haversine forumla to calculate the distance between the two points. More
        // information can be found here: https://en.wikipedia.org/wiki/Haversine_formula

        double earthRadiusMeters = 6371000;

        double latitudeDistance = Math.toRadians(point2.latitude - point1.latitude);
        double longitudeDistance = Math.toRadians(point2.longitude - point1.longitude);

        double point1Latitude = Math.toRadians(point1.latitude);
        double point2Latitude = Math.toRadians(point2.latitude);

        double sideA = Math.sin(latitudeDistance/2.0) * Math.sin(latitudeDistance/2.0) +
                Math.sin(longitudeDistance/2.0) * Math.sin(longitudeDistance/2.0) *
                        Math.cos(point1Latitude) * Math.cos(point2Latitude);
        double angleOppositeC = 2 * Math.atan2(Math.sqrt(sideA), Math.sqrt(1-sideA));

        return earthRadiusMeters * angleOppositeC;
    }

    /**
     * Finds the bearing between two GPS coordinates in radians
     * @param point1 start point
     * @param point2 end point
     * @return bearing relative to north in radians from point 1 to point 2
     */
    public static double bearingBetween(LatLng point1, LatLng point2){
        double longitudeDistance = Math.toRadians(point2.longitude - point1.longitude);
        double point1Latitude = Math.toRadians(point1.latitude);
        double point2Latitude = Math.toRadians(point2.latitude);

        double y = Math.sin(longitudeDistance)*Math.cos(point2Latitude);
        double x = Math.cos(point1Latitude)*Math.sin(point2Latitude)
                 - Math.sin(point1Latitude)*Math.cos(point2Latitude)*Math.cos(longitudeDistance);

        return Math.atan2(y,x);
    }

    /**
     * Forces navigation to progress to the next point on the path even if GPS doesn't indicate that
     * the player has reached the current point.
     */
    public void toNextPointOnPath() {
        if (path != null) {
            path.remove(0);
        } else {
            Log.w(TAG, "The path is null when calling toNextPointOnPath!");
        }
    }

    /**
     * Switches to tour mode. (This replaces the current path with the tour path)
     */
    public void enableTour(){
        tourMode = true;
        path = null;
        destination = null;
        attemptCalculatePath();
    }

    /**
     * Switches back to normal navigation mode. Should only be called from main activity or if a destination is already set.
     * Otherwise, this will
     */
    public void disableTour(){
        tourMode = false;
        path = null;
        destination = null;
    }


    /**
     * Sets destination and disables tour
     * @param dest location of the destination
     * @return true
     */
    public boolean setDestination(LatLng dest){
        disableTour();
        destination = dest;
        attemptCalculatePath();
        return true;
    }

    public LatLng getDestination(){

        return destination;
    }

    /**
     * Sets desintation based on a building name and disables tour
     * @param destination name of building for destination
     * @return if the building was added as destination
     */
    public boolean setDestination(String destination){
        disableTour();
        LatLng initDestination = getBuilding(destination).getLocation();

        if(initDestination == null){
            return false;
        }
        setDestination(initDestination);
        return true;
    }

    /**
     *
     * @return if the user has arrived at their destination
     */
    public boolean hasArrivedAtDestination(){
        LatLng nextPos = getPointOnPath(0);
        return destination != null && nextPos != null && path.size() == 1 && distanceBetweenInMeters(nextPos, currentPosition) < PATH_LOCATION_COMPLETED_THRESHOLD;
    }


    /**
     * Returns the current position if it's available.
     * @return an Optional for the position
     */
    public Optional<LatLng> getCurrentPosition(){
        return Optional.ofNullable(currentPosition);
    }

    /**
     * @return the indexed point on the path. Will return the next point in the tour if tour mode is enabled.
     * Returns null if a path hasn't been calculated yet or the index is out of range
     * @param index
     */
    public LatLng getPointOnPath(int index) {

        if (path != null && path.size() >= index + 1) {
            return path.get(index);
        }

        return null;
    }

    /**
     * @return a list of LatLng positions along the path. Will return the tour path if tour mode is enabled. Includes the current position as the first point on the path.
     */
    public ArrayList<LatLng> getPath(){
        if(path != null && path.size() >= 1 && currentPosition != null){
            ArrayList<LatLng> currentPath = (ArrayList<LatLng>)path.clone();
            currentPath.add(0, currentPosition);
            return currentPath;
        }
        return null;
    }

    /**
     * @param bld the name of the building
     * @return the position of the building
     */
    public Building getBuilding(String bld){
        return buildings.get(bld);
    }
    /**
     * @return the list of building names
     */
    List<String> getBuildingNames() {
        return new ArrayList<>(buildings.keySet());
    }


    /**
     * Adds a LocationUpdateListener to the set of listeners to update upon location change
     * @param l the listener to add
     */
    public void subscribeToLocationUpdates(LocationUpdateListener l){
        listeners.add(l);
    }


    /**
     * Returns all points of interest.
     * @return list of points of interest
     */
    public List<PointOfInterest> getPointsOfInterest(){

        boolean poiEnabled = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("poiEnabled",false);
        if(!poiEnabled)
            return new ArrayList<PointOfInterest>();

        return pointsOfInterest;
    }

    /**
     * Returns the num_points closest points of interest near me
     * @return list of points of interest
     */
    public List<PointOfInterest> getPointsOfInterest(int numPoints){
        ArrayList<PointOfInterest> points = new ArrayList<PointOfInterest>();

        boolean poiEnabled = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("poiEnabled",false);
        if(!poiEnabled)
            return points;

        ArrayList<PointOfInterest> oldPoints = (ArrayList<PointOfInterest>) pointsOfInterest.clone();
        if(currentPosition != null) {
            while (points.size() < numPoints && oldPoints.size() > 0) {
                int smallestInd = -1;
                double smallestDist = Double.MAX_VALUE;
                for (int i = 0; i < oldPoints.size(); i++) {
                    double pDist = distanceBetweenInMeters(oldPoints.get(i).getLocation(), currentPosition);
                    if (pDist < smallestDist) {
                        smallestDist = pDist;
                        smallestInd = i;
                    }
                }
                points.add(oldPoints.get(smallestInd));
                oldPoints.remove(smallestInd);
            }
        }
        return points;
    }

    /**
     * Returns the points of interest within POINTS_OF_INTEREST_NEAR_ME_RADIUS
     * @return list of points of interest
     */
    public List<PointOfInterest> getPointsOfInterestNearMe(){
        ArrayList<PointOfInterest> points = (ArrayList<PointOfInterest>) pointsOfInterest.clone();

        boolean poiEnabled = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("poiEnabled",true);
        if(!poiEnabled)
            return points;

        if(currentPosition != null) {
            for(int i = points.size()-1;i>=0;i--){
                if(distanceBetweenInMeters(points.get(i).getLocation(), currentPosition) > POINTS_OF_INTEREST_NEAR_ME_RADIUS){
                    points.remove(i);
                }
            }
        }
        return points;
    }

    /**
     * Returns a point of interest with the given name, or null if none exists.
     * @param name name to search for a POI with
     * @return the POI or null
     */
    PointOfInterest getPointOfInterest(String name){
        for(PointOfInterest poi: pointsOfInterest){
            if(poi.getName().equals(name)){
                return poi;
            }
        }
        return null;
    }


    boolean getTourMode(){
        return tourMode;
    }

    /**
     * Finds all points of interest that the user is currently facing.
     * @param sensorHelper a SensorHelper for getting the user's current heading
     * @return all points of interest the user is facing
     */
    List<PointOfInterest> getFacingPointsOfInterest(SensorHelper sensorHelper) {
        List<PointOfInterest> result = new ArrayList<>();

        boolean poiEnabled = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("poiEnabled",false);
        if(!poiEnabled)
            return result;

        Optional<LatLng> location = getCurrentPosition();

        // We're not facing anything if we don't know our location yet
        if (!location.isPresent()) {
            return result;
        }

        for (PointOfInterest poi : getPointsOfInterest()) {
            // Find the angle between the user and the POI
            double bearing = bearingBetween(location.get(), poi.getLocation());
            // Find the borders in the user's field of view
            double leftmostFieldOfView = (sensorHelper.getHeading() - (FIELD_OF_VIEW/2.0)) % (Math.PI*2.0);
            double rightmostFieldOfView = (sensorHelper.getHeading() + (FIELD_OF_VIEW/2.0)) % (Math.PI*2.0);

            if (MathUtils.isAngleBetween(bearing, leftmostFieldOfView, rightmostFieldOfView)) {
                result.add(poi);
            }
        }

        return result;
    }

    //helper method for astar
    private LatLng lowestScore(ArrayList<LatLng> nodes, Hashtable<LatLng,Double> costs){
        double lScore = -1;
        LatLng lLl = null;
        for(LatLng ll:nodes){
            if(costs.get(ll) < lScore || lLl == null){
                lLl = ll;
                lScore = costs.get(ll);
            }
        }
        return lLl;
    }

    //helper method for astar
    private Hashtable<LatLng,Double> initMax(Iterable<LatLng> vals){
        Hashtable<LatLng,Double> res = new Hashtable<LatLng, Double>();
        for(LatLng l: vals)
            res.put(l,Double.MAX_VALUE);
        return res;
    }
    //helper method for astar
    private ArrayList<LatLng> reconstructPath(Hashtable<LatLng,LatLng> mostEff, LatLng endPos){
        ArrayList<LatLng> path = new ArrayList<LatLng>();
        LatLng current = endPos;
        path.add(current);
        while(contained(mostEff,current)){
            current = mostEff.get(current);
            path.add(current);
        }
        return path;
    }
    //helper method for a*
    private boolean contained(Hashtable<LatLng,LatLng> vals, LatLng target){
        for(LatLng k:vals.keySet()){
            if(k.latitude == target.latitude && k.longitude == target.longitude){
                return true;
            }
        }

        return false;
    }
    /**
     * Returns the shortest path between two positions using nodes in 'edges'
     * @param start_c the starting position
     * @param goal_c the ending position
     * @return a path of LatLng points from destination to target
     */
    private ArrayList<LatLng> astar(LatLng start_c, LatLng goal_c){
        //find closest vertex to start and goal
        LatLng start = null;
        LatLng goal = null;
        for(LatLng l:edges.keySet()){
            if(start == null || distanceBetweenInMeters(l,start_c) < distanceBetweenInMeters(start,start_c)){
                start=l;
            }
            if(goal == null || distanceBetweenInMeters(l,goal_c) < distanceBetweenInMeters(goal,goal_c)){
                goal=l;
            }
        }
        ArrayList<LatLng> evalSet = new ArrayList<LatLng>(); //set of nodes already evaluated
        ArrayList<LatLng> discNotEval = new ArrayList<LatLng>(); //set of discovered, unevaluated nodes
        discNotEval.add(start);
        Hashtable<LatLng,LatLng> mostEff = new Hashtable<LatLng,LatLng>(); //maps each node to the node it can be most efficiently reached from
        Hashtable<LatLng,Double> fromStartCost = initMax(edges.keySet()); //cost of getting from start to node
        fromStartCost.put(start,0.0);
        Hashtable<LatLng,Double> toGoalCost = initMax(edges.keySet()); //maps each node to the cost of getting from start to goal by passing through that node
        toGoalCost.put(start,distanceBetweenInMeters(start,goal));
        while(discNotEval.size() > 0){

            LatLng current = lowestScore(discNotEval,toGoalCost);
            if(current.equals(goal)) {
                ArrayList<LatLng> path = reconstructPath(mostEff,current);
                //path.add(start_c);
                path.add(0,goal_c);
                ArrayList<LatLng> reversed = new ArrayList<LatLng>();
                for(int i = 0;i<path.size();i++){
                    reversed.add(0,path.get(i));
                }
                return reversed;
            }
            discNotEval.remove(current);
            evalSet.add(current);
            for(LatLng neighbor: edges.get(current)){
                if(evalSet.contains(neighbor)){
                    continue;
                }
                if(!discNotEval.contains(neighbor)){
                    discNotEval.add(neighbor);
                }
                /*
                if(!most_eff.contains(neighbor)){
                    most_eff.put(neighbor,current);
                }
                */
                double tentativeScore = fromStartCost.get(current) + distanceBetweenInMeters(current,neighbor);
                if(tentativeScore >= fromStartCost.get(neighbor)){
                    continue;
                }
                mostEff.put(neighbor,current);
                fromStartCost.put(neighbor,tentativeScore);
                toGoalCost.put(neighbor,fromStartCost.get(neighbor)+distanceBetweenInMeters(neighbor,goal));
            }
        }
        Log.i("failed to calculate path", "failed to calculate path");
        return null;
    }

    private LatLng jsonLatLng(JSONObject o) throws JSONException {
        return new LatLng(o.getDouble("lat"),o.getDouble("lng"));
    }
    //loads a JSONObject from a raw resource file
    private JSONObject loadJSONFile(int resourceName){
        InputStream is = cont.getResources().openRawResource(resourceName);
        StringWriter writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        try{
            is.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        try {
            return new JSONObject(writer.toString());
        }
        catch(Exception e){}
        return null;

    }
    //loads all data for navigator from resource files
    private void loadData(){
        JSONObject buildingsJs = loadJSONFile(R.raw.buildings);
        JSONObject edgesJs = loadJSONFile(R.raw.edges);
        JSONObject tourJs = loadJSONFile(R.raw.tour_locations);
        JSONObject pOI = loadJSONFile(R.raw.points_of_interest);


        buildings = new Hashtable<String,Building>();
        buildingsTable = new Hashtable<String,Hashtable<String,Building>>();

        edges = new Hashtable<LatLng,ArrayList<LatLng>>();
        edgeTable = new Hashtable<String,Hashtable<LatLng,ArrayList<LatLng>>>();

        pointsOfInterest = new ArrayList<>();
        pointsOfInterestTable = new Hashtable<String,ArrayList<PointOfInterest>>();

        tour = new ArrayList<LatLng>();
        tourTable = new Hashtable<String,ArrayList<LatLng>>();


        try {
            //load tour locations
            Iterator<String> tourCampuses = tourJs.keys();
            while(tourCampuses.hasNext()){
                String campus = tourCampuses.next();
                JSONArray tourLocs = tourJs.getJSONArray(campus);
                tourTable.put(campus,new ArrayList<LatLng>());
                for (int i = 0; i < tourLocs.length(); i++) {
                    tourTable.get(campus).add(jsonLatLng(tourLocs.getJSONObject(i)));
                }
            }

            //load buildings
            Iterator<String> buildingsCampuses = buildingsJs.keys();
            while(buildingsCampuses.hasNext()){
                String campus = buildingsCampuses.next();
                buildingsTable.put(campus,new Hashtable<String,Building>());
                JSONObject campusBuildings = buildingsJs.getJSONObject(campus);
                Iterator<String> buildingIter = campusBuildings.keys();
                while (buildingIter.hasNext()) {
                    String name = buildingIter.next();
                    JSONObject coord = campusBuildings.getJSONObject(name);
                    buildingsTable.get(campus).put(name, new Building(name, coord));
                }
            }

            //load points of interest
            Iterator<String> poiCampuses = pOI.keys();
            while(poiCampuses.hasNext()){
                String campus = poiCampuses.next();
                JSONArray pois = pOI.getJSONArray(campus);
                ArrayList<PointOfInterest> points = new ArrayList<PointOfInterest>();
                for (int i = 0; i < pois.length(); i++) {
                    JSONObject jsonPoi = pois.getJSONObject(i);
                    points.add(new PointOfInterest(jsonPoi));
                }
                pointsOfInterestTable.put(campus,points);
            }


            //load edges
            Iterator<String> campusIter = edgesJs.keys();
            while (campusIter.hasNext()) {
                String campus = campusIter.next();
                edgeTable.put(campus,new Hashtable<LatLng,ArrayList<LatLng>>());
                Hashtable<String, LatLng> vertices = new Hashtable<String, LatLng>();
                Hashtable<String, ArrayList<String>> edgeSet = new Hashtable<String, ArrayList<String>>();
                JSONObject campusJs = edgesJs.getJSONObject(campus);
                Iterator<String> edgeIter = campusJs.keys();
                while (edgeIter.hasNext()) {
                    String key = edgeIter.next();
                    JSONObject coord = campusJs.getJSONObject(key);
                    vertices.put(key, new LatLng(coord.getDouble("lat"), coord.getDouble("lng")));
                    ArrayList<String> edgeList = new ArrayList<String>();
                    JSONArray edgeListJs = coord.getJSONArray("edges");
                    for (int i = 0; i < edgeListJs.length(); i++) {
                        edgeList.add(edgeListJs.getString(i));
                    }
                    edgeSet.put(key, edgeList);
                }
                for (String key : vertices.keySet()) {
                    ArrayList<LatLng> edgeVerts = new ArrayList<LatLng>();
                    for (String s : edgeSet.get(key)) {
                        edgeVerts.add(vertices.get(s));
                    }
                    edgeTable.get(campus).put(vertices.get(key), edgeVerts);
                }
            }
        }
        catch(JSONException e){
            e.printStackTrace();
            Log.i("failed to load data","failed to load data");
        }
    }

    /**
     * @return List of full building names
     */
    List<String> getBuildingFullNames() {
        ArrayList<String> bl = new ArrayList<String>();
        for (String key: buildings.keySet()) {
            bl.add(buildings.get(key).getName());
        }
        Collections.sort(bl);
        return bl;
    }


    /*
    * @returns: String
    * Returns a building code when you pass a building name
    * */
    public String getBuildingCode(String name){
        for(String k: buildings.keySet()){
            if(buildings.get(k).getName().equals(name)){
                return k;
            }
        }
        return null;
    }

    public Boolean isFacingPOI(SensorHelper sensorHelper) {
        List<PointOfInterest> result = new ArrayList<>();

        boolean poiEnabled = PreferenceManager.getDefaultSharedPreferences(cont).getBoolean("poiEnabled",false);
        if(!poiEnabled)
            return false;

        Optional<LatLng> location = getCurrentPosition();

        // We're not facing anything if we don't know our location yet
        if (!location.isPresent()) {
            return false;
        }
        List<PointOfInterest> poi1 = getPointsOfInterestNearMe();

        for (PointOfInterest poi : getPointsOfInterestNearMe()) {
            // Find the angle between the user and the POI
            double bearing = bearingBetween(location.get(), poi.getLocation());
            // Find the borders in the user's field of view
            double leftmostFieldOfView = (sensorHelper.getHeading() - (FIELD_OF_VIEW/2.0)) % (Math.PI*2.0);
            double rightmostFieldOfView = (sensorHelper.getHeading() + (FIELD_OF_VIEW/2.0)) % (Math.PI*2.0);

            if (MathUtils.isAngleBetween(bearing, leftmostFieldOfView, rightmostFieldOfView)) {
                poiName = poi.getName();
                return true;
            }
            else{
                poiName = null;
            }
        }

        return false;
    }

    public String getPOIName(){
        return poiName;
    }


}
