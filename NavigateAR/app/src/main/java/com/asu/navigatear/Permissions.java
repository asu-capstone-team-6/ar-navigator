package com.asu.navigatear;

import android.Manifest;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Provides helper methods for managing permissions.
 */
class Permissions {
    /**
     * Identifies our permission request.
     */
    static final int CODE = 1;

    /**
     * All required permissions for this app.
     */
    private static final String[] REQUIRED_PERMISSIONS = new String[]{
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CAMERA
    };

    /**
     * Returns true if the activity has all required permissions.
     *
     * @param activity the current activity
     * @return true if permissions granted
     */
    static boolean hasPermissions(Activity activity) {
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    /**
     * Sends the user a request to access the required parameters. The activity that calls this
     * method should implement ActivityCompat.OnRequestPermissionsResultCallback so that it can
     * respond if permissions are not granted.
     *
     * @param activity the current activity
     */
    static void requestMissingPermissions(Activity activity) {
        ActivityCompat.requestPermissions(activity, REQUIRED_PERMISSIONS, CODE);
    }
}
