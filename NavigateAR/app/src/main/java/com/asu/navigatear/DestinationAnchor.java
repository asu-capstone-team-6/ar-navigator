package com.asu.navigatear;

import com.google.ar.core.Anchor;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;

/**
 * A class to increase the accuracy of anchors. 
 * Created by Paul Horton on 2/26/18.
 */
public class DestinationAnchor {
    private Anchor outputCurrAnchor;
    private Anchor updatingCurrAnchor;
    private Anchor outputNextAnchor;
    private Anchor updatingNextAnchor;
    private boolean nullNextAnchor;
    private int currConfidence;
    private int nextConfidence;

    /**
     *     At what value of n should we hold it for all future updates
     */
    private final int THRESHOLD = 500;

    /**
     * The amount of frames to wait before updating the output anchor.
     */
    private final int UPDATE_RATE = 30;

    /**
     * Used to keep track of how many frames have passed between updates.
     */
    private int frameCount;

    /**
     * Constructor for a destination anchor, use this as the initial guess to where
     * the anchor point should be placed for current and next anchor.
     * @param currAnchor
     * @param nextAnchor
     */
    public DestinationAnchor(Anchor currAnchor, Anchor nextAnchor){
        this.outputCurrAnchor = currAnchor;
        this.updatingCurrAnchor = currAnchor;
        this.outputNextAnchor = nextAnchor;
        this.updatingNextAnchor = nextAnchor;
        this.nullNextAnchor = (nextAnchor == null);
        currConfidence = 2;
        nextConfidence = 2;
    }

    /**
     * Updates the value of the anchors. This will ensure that a cumulative running average
     * is kept at all times
     * @param mSession The session to create anchors in
     * @param currPose The calculated pose estimate of the current destination
     * @param nextPose The calculated pose estimate of the next destination
     */
    public void update(Session mSession, Pose currPose, Pose nextPose){
        Pose currLast = updatingCurrAnchor.getPose();

        float[] currLastQuat = new float[4];
        currLast.getRotationQuaternion(currLastQuat,0);
        float[] currConfTran = new float[] {
            (currPose.tx() - currLast.tx())/(currConfidence-1) + currLast.tx(),
            (currPose.ty() - currLast.ty())/(currConfidence-1) + currLast.ty(),
            (currPose.tz() - currLast.tz())/(currConfidence-1) + currLast.tz()};
        updatingCurrAnchor = mSession.createAnchor(new Pose(currConfTran,currLastQuat));
        if(currConfidence < THRESHOLD) {
            currConfidence ++;
        }
        if(!nullNextAnchor) {
            Pose nextLast = updatingNextAnchor.getPose();
            float[] nextLastQuat = new float[4];
            nextLast.getRotationQuaternion(nextLastQuat, 0);
            float[] nextConfTran = new float[]{
                    (nextPose.tx() - nextLast.tx()) / (nextConfidence - 1) + nextLast.tx(),
                    (nextPose.ty() - nextLast.ty()) / (nextConfidence - 1) + nextLast.ty(),
                    (nextPose.tz() - nextLast.tz()) / (nextConfidence - 1) + nextLast.tz()};
            updatingNextAnchor = mSession.createAnchor(new Pose(nextConfTran, nextLastQuat));
            if (nextConfidence < THRESHOLD) {
                nextConfidence++;
            }
        }

        // Check if it's time to update the output anchor
        frameCount = (frameCount + 1) % UPDATE_RATE;
        if (frameCount == 0) {
            outputCurrAnchor = updatingCurrAnchor;
            outputNextAnchor = updatingNextAnchor;
        }
    }

    /**
     * Gets the current anchor
     * @return the cumulative running average of the current anchor
     */
    public Anchor getCurrAnchor(){
        return outputCurrAnchor;
    }

    /**
     * Gets the next anchor
     * @return the cumulative running average of the next anchor
     */
    public Anchor getNextAnchor(){
        return outputNextAnchor;
    }


    /**
     * Updates the current destination to the previous next and
     * makes a new next destination
     * @param nextAnchor The new next anchor to look
     */
    public void nextDestination(Anchor nextAnchor) {
        if(nextAnchor == null){
            this.nullNextAnchor = true;
        }
        updatingCurrAnchor = updatingNextAnchor;
        outputCurrAnchor = outputNextAnchor;
        currConfidence = nextConfidence;

        updatingNextAnchor = nextAnchor;
        outputNextAnchor = nextAnchor;
        nextConfidence = 2;
    }

    /**
     * Returns the distance between the given location and the output location of the
     * DestinationAnchor.
     * @param location the location to test against
     * @return distance in meters
     */
    public double distanceToCurrent(Pose location) {
        Pose outPose = outputCurrAnchor.getPose();

        return Math.sqrt(
                Math.pow(location.tz() - outPose.tz(), 2) +
                Math.pow(location.tx() - outPose.tx(), 2));
    }

}
