package com.asu.navigatear.rendering;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.asu.navigatear.MathUtils;
import com.asu.navigatear.Navigator;
import com.asu.navigatear.R;
import com.asu.navigatear.WorldState;
import com.google.ar.core.Anchor;
import com.google.ar.core.Pose;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Renders the 3D model indicating the destination point in AR, a large platform with a spinning
 * outline.
 */
public class DestinationRenderer implements Renderable {
    private static final String TAG = DestinationRenderer.class.getSimpleName();

    /**
     * The amount to rotate the border by every frame. Expressed as a quaternion.
     */
    private static final float[] ROTATION_AMOUNT = {0.0f, 0.0087f, 0.0f, 0.999f};

    private ObjectRenderer destination;
    private ObjectRenderer destinationBorder;
    private ObjectRenderer finalDestinationPin;

    private float[] borderRotation;

    public DestinationRenderer() {
        destination = new ObjectRenderer();
        destinationBorder = new ObjectRenderer();
        finalDestinationPin = new ObjectRenderer();

        destination.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
        destinationBorder.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);
        finalDestinationPin.setMaterialProperties(0.0f, 3.5f, 1.0f, 6.0f);

        borderRotation = new float[]{0.0f, 0.0f, 0.0f, 1.0f};
    }

    public void createOnGlThread(Context context) throws IOException {
        destination.createOnGlThread(context,
                "destination.obj",
                "destination.png");
        destinationBorder.createOnGlThread(context,
                "destination-border.obj",
                "destination.png");
        finalDestinationPin.createOnGlThread(context,
                "pin-destination.obj",
                "pin-destination.png");
    }

    /**
     * Positions the destination model at the given pose.
     * @param pose the pose to position at
     */
    private void atPose(Pose pose) {
        float[] anchorMatrix = new float[16];
        pose.toMatrix(anchorMatrix, 0);
        destination.updateModelMatrix(anchorMatrix, 0.5f);

        float[] borderTranslation = new float[3];
        pose.getTranslation(borderTranslation, 0);
        borderRotation = MathUtils.multiplyQuaternions(borderRotation, ROTATION_AMOUNT);
        Pose borderPose = new Pose(borderTranslation, borderRotation);
        borderPose.toMatrix(anchorMatrix, 0);
        destinationBorder.updateModelMatrix(anchorMatrix, 0.5f);

        finalDestinationPin.updateModelMatrix(anchorMatrix, 10.0f);
    }

    /**
     * Draws a platform at the destination.
     * @param ctx the current Android context
     * @param worldState the current AR state
     */
    public void draw(Context ctx, WorldState worldState) {
        Navigator navigator = Navigator.getInstance(ctx);
        if (worldState.getFloorPlane() == null || !navigator.getCurrentPosition().isPresent()
                || worldState.getCurrDestinationPose() == null) {
            return;
        }

        atPose(worldState.getCurrDestinationPose());

        float[] cameramtx = worldState.getCameraViewMatrix();
        float[] projectionmtx = worldState.getProjectionMatrix();
        float lightIntensity = worldState.getLightIntensity();

        destination.draw(cameramtx, projectionmtx, lightIntensity);
        destinationBorder.draw(cameramtx, projectionmtx, lightIntensity);
        // Draw a final destination pin over it if it's the last point on the path
        if (worldState.getIsCurrentFinal()) {
            finalDestinationPin.draw(cameramtx, projectionmtx, lightIntensity);
        }
    }
}
