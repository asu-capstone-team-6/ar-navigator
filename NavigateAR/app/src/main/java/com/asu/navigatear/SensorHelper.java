package com.asu.navigatear;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.widget.Toast;

public class SensorHelper implements SensorEventListener {
    private float heading;
    private SensorManager sensorManager;
    private Sensor gravity;
    private Sensor magnet;
    private float[] magnetIntensity;
    private float[] gravityIntensity;
    private int counterM = 0;
    private int counterG = 0;
    private float[][] magMovingAve;
    private float[][] gravMovingAve;

    public SensorHelper(){
    }

    public SensorHelper(Context context){
        sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        magnet = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        gravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        if(magnet != null && gravity != null){
            sensorManager.registerListener(this, magnet, SensorManager.SENSOR_DELAY_NORMAL);
            sensorManager.registerListener(this, gravity, SensorManager.SENSOR_DELAY_NORMAL);
            // Make sure sensors are available
            if (!checkSensor())
                Toast.makeText(context, "Your phone will not support navigation functions needed by this app", Toast.LENGTH_LONG).show();
        }
        gravMovingAve = new float[10][3];
        magMovingAve = new float[10][3];
    }

    public float getHeading(){
        return heading;
    }
    public String toString(){
        return Float.toString(heading);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        // Load values from sensors
        /*if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            magnetIntensity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_GRAVITY)
            gravityIntensity = event.values;
        */
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magMovingAve[counterM] = event.values;
            counterM = (counterM+1)%10;
        }
        if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
            gravMovingAve[counterG] = event.values;
            counterG = (counterG+1)%10;
        }

        magnetIntensity = movingAve(magMovingAve);
        gravityIntensity = movingAve(gravMovingAve);

        // Next steps, make sure neither is full of null values (ie newly init)
        if (magnetIntensity != null && gravityIntensity !=  null){
            float rotation[] = new float[9];
            float inclination[] = new float[9];
            // Calculate rotation and inclination from magnet and gravity
            boolean worked = sensorManager.getRotationMatrix(rotation, inclination, gravityIntensity, magnetIntensity);
            if (worked){
                float orientation[] = new float[3];
                sensorManager.getOrientation(rotation, orientation);
                float azimuth = orientation[0]; // azimuth is horizontal angle of a compass, [1] is pitch, [2] is roll

                //Further calibration of true north is unnecessary given our location and the scope of
                // this project, on the off chance it is needed this will remain
                // Need geometric to figure out declination
                /*Location location = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double lat = location.getLatitude(),
                    longitude = location.getLongitude(),
                        alt = location.getAltitude();
                GeomagneticField geo = new GeomagneticField((float) lat, (float) longitude, (float) alt, System.currentTimeMillis());*/
                // need to subtract declination from existing angle to determine true north, true north dependent on time and location
                //azimuth -= geo.getDeclination();

                //azimuth *= (180/Math.PI);
                heading = azimuth;
            }
        }
    }

    private float[] movingAve(float[][] input){
        float[] ave = new float[3];
        for (int i = 0; i < input.length; i++){
            for (int j = 0; j < input[i].length; j++){
                ave[j] += input[i][j];
            }
        }
        for (int i = 0; i < 3; i++)
            ave[i]/=10.0;
        return ave;
    }

    private boolean checkSensor() { // Check phone has sensors available
        return ((sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) &&
                (sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null));
    }


    public void onResume(){
        sensorManager.registerListener(this, magnet, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, gravity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void onPause(){
        sensorManager.unregisterListener(this);
    }

}