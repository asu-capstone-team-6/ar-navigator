package com.asu.navigatear;

import com.google.android.gms.maps.model.LatLng;
import com.google.ar.core.Pose;

/**
 * A utility class providing stateless AR operations.
 */
public class ARUtils {

    /**
     * Finds a pose dist distance away at a heading of rad radians.
     * @param camPose the pose of the Camera
     * @param floorPose the pose of the Floor
     * @param dist the distance to the endpoint
     * @param rad the radians towards the endpoint
     * @return a pose relative to the camera and floor.
     * TODO: Rotate to face user.
     */
    public static Pose getOffsetPose(Pose camPose, Pose floorPose, float dist, float rad){
        // Unused for now, it would be nice to get the anchor to face the user by only rotating
        // about the Y axis.
        //float[] camQuat = new float[4];
        //float[] offsetQuat;

        // Instantiate float arrays to hold the information about the camera and floor
        float[] camTran = new float[3];
        float[] floorTran = new float[3];
        float[] floorQuat = new float[4];

        // The translation of the end point
        float[] offsetTran;

        // Get a unit projection of the phone's lens axis
        // in the XZ (flat) plane, rotated rad degrees.
        float[] unit2DVector = rotateXZVector(getXZProjection(camPose.getYAxis()),rad);

        // Get the translation information for the camera
        camPose.getTranslation(camTran,0);
        // Holding off on this (see above)
        //camPose.getRotationQuaternion(camQuat,0);

        // Get the translation and rotation information for the floor
        floorPose.getTranslation(floorTran,0);
        floorPose.getRotationQuaternion(floorQuat,0);

        // Get the new translation by using the location of the camera
        // offset by a distance in the direction of the lens (and offset radians).
        // We are using the floor's Y axis to place the item on the ground
        offsetTran = new float[]{
                camTran[0] + unit2DVector[0]*dist,
                floorTran[1],
                camTran[2] + unit2DVector[2]*dist
        };
        // Return the value of the translation and the floors rotation
        // In the future we will replace floor quat with a new rotation relative to the camera.
        return new Pose(offsetTran,floorQuat);
    }

    /**
     * Gets the projection of a vector on the XZ plane
     * @param vector the initial X Y Z vector
     * @return a unit vector in the direction of the XYZ vector in the XZ plane
     */
    private static float[] getXZProjection(float[] vector){
        float amplitude = (float)Math.sqrt(Math.pow(vector[0],2) + Math.pow(vector[2],2));
        return new float[]{vector[0]/amplitude,0,vector[2]/amplitude};
    }


    /**
     * Rotates a vector a given degree
     * @param vector the initial X Y Z vector
     * @param rad the angle to rotate the vector through (CW)
     * @return a vector rotated about the Y axis by rad degrees.
     */
    private static float[] rotateXZVector(float[] vector, float rad){
        // Gets the cos and sin values
        float rCos = (float)Math.cos(rad);
        float rSin = (float)Math.sin(rad);
        // Matrix multiplication expanded out
        return new float[]{
                vector[0]*rCos - vector[2]*rSin,
                vector[1],
                vector[0]*rSin + vector[2]*rCos
        };
    }

    /**
     * Returns the user's current bearing relative to their heading.
     * @param start the user's current position
     * @param end the user's destination
     * @param heading the user's current heading
     * @return bearing
     */
    public static float getBearing(LatLng start, LatLng end, float heading){
        float bearing = (float) Navigator.bearingBetween(start,end);
        return bearing - heading;
    }

}
