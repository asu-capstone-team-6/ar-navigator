/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.asu.navigatear;

import android.content.Intent;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.Button;

import com.asu.navigatear.rendering.ArrowRenderer;
import com.asu.navigatear.rendering.DestinationRenderer;
import com.asu.navigatear.rendering.POIRenderer;
import com.asu.navigatear.rendering.UserLocationRenderer;
import com.asu.navigatear.rendering.WorldRenderer;
import com.google.android.gms.maps.model.LatLng;
import com.google.ar.core.Anchor;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Config;
import com.google.ar.core.Plane;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.asu.navigatear.rendering.BackgroundRenderer;
import com.asu.navigatear.rendering.LineRenderer;
import com.asu.navigatear.rendering.PlaneRenderer;
import com.asu.navigatear.rendering.PointCloudRenderer;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using the
 * ARCore API. The application will display any detected planes and will allow the user to tap on a
 * plane to place a 3d model of the Android robot.
 */
public class CameraActivity extends AppCompatActivity implements GLSurfaceView.Renderer {

    private static final double WAYPOINT_THRESHOLD = 4.5;
    // Value from sensors
    private SensorHelper sensorHelper;

    // Location information
    private LatLng pastCurr = null;
    private LatLng pastNext = null;

    private boolean messageShown = false;

    private static final String TAG = CameraActivity.class.getSimpleName();

    private boolean fabsVisible = false;

    /**
     * The max distance to draw the line before limiting its size. When the destination is farther
     * away than this distance, the line will be limited to this size.
     */
    private final float MAX_LINE_DISTANCE_METERS = 30.0f;

    // Rendering. The Renderers are created here, and initialized when the GL surface is created.
    private GLSurfaceView mSurfaceView;

    /**
     * True if we want to ask the user to install ARCore.
     */
    private boolean requestToInstallArCore;

    private Session mSession;
    private GestureDetector mGestureDetector;
    private Snackbar mMessageSnackbar;
    private DisplayRotationHelper mDisplayRotationHelper;

    private WorldRenderer worldRenderer = new WorldRenderer();

    // Temporary matrix allocated here to reduce number of allocations for each frame.
    private final float[] mAnchorMatrix = new float[16];

    // Tap handling and UI.
    private final ArrayBlockingQueue<MotionEvent> mQueuedSingleTaps = new ArrayBlockingQueue<>(16);
    private DestinationAnchor destAnchor;

    /**
     * Our interface to the navigation subsystem.
     */
    private Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        mSurfaceView = findViewById(R.id.surfaceview);
        mDisplayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

        // Set up tap listener.
        mGestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                onSingleTap(e);
                return true;
            }

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            public boolean onDoubleTap(MotionEvent e){
                doubleTapped(e);
                return true;
            }
        });

        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });

        // Set up renderer.
        mSurfaceView.setPreserveEGLContextOnPause(true);
        mSurfaceView.setEGLContextClientVersion(2);
        mSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        mSurfaceView.setRenderer(this);
        mSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

        Toolbar tbar =
                (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tbar);

        // Get a support ActionBar corresponding to this toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // code for button

        Button button1 = (Button) findViewById(R.id.button2);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TextView text   = (TextView)findViewById(R.id.copyright);
                //dest1 = text.getText().toString();
                Intent map = new Intent(CameraActivity.this, MapsActivity.class);
                //map.putExtra("destination", dest1);
                startActivity(map);

            }
        });

        FloatingActionButton refreshButt = findViewById(R.id.refreshFAB);
        FloatingActionButton consumeButt = findViewById(R.id.consumeFAB);
        refreshButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(destAnchor != null){
                    destAnchor = null;
                }
            }
        });
        consumeButt.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                navigator.toNextPointOnPath();
            }
        });


        sensorHelper = new SensorHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSession == null) {
            boolean success = initARSession();
            if (!success) {
                return; // Initialization has not finished yet
            }
        }

        // All is good, resume everything
        sensorHelper.onResume();
        if (mSession != null) {
            showLoadingMessage();
            mSession.resume();
        }

        mSurfaceView.onResume();
        mDisplayRotationHelper.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        // Sensor
        sensorHelper.onPause();
        // Note that the order matters - GLSurfaceView is paused first so that it does not try
        // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
        // still call mSession.update() and get a SessionPausedException.
        mDisplayRotationHelper.onPause();
        mSurfaceView.onPause();
        if (mSession != null) {
            mSession.pause();
        }
    }

    @Override
    public void onBackPressed(){
        navigator = Navigator.getInstance(this);
        Intent intent =  new Intent(CameraActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    private void onSingleTap(MotionEvent e) {
        // Queue tap if there is space. Tap is lost if queue is full.
        mQueuedSingleTaps.offer(e);
        navigator = Navigator.getInstance(this);
        String poiName = navigator.getPOIName();
        if(poiName != null){
            String toastText = "Facing: " +  poiName + " Double tap for info";
            Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
        }
    }

    private void doubleTapped(MotionEvent e){
        String toastText = "Double tapped ";
        navigator = Navigator.getInstance(this);
        String poiName = navigator.getPOIName();
        if(poiName != null){
            Intent poi = new Intent(CameraActivity.this, PoiActivity.class);
            poi.putExtra("point of interest name", poiName);
            startActivity(poi);
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        // Create the texture and pass it to ARCore session to be filled during update().
        BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
        backgroundRenderer.createOnGlThread(/*context=*/ this);
        if (mSession != null) {
            mSession.setCameraTextureName(backgroundRenderer.getTextureId());
        }
        worldRenderer.add(backgroundRenderer);

        // Prepare the other rendering objects.
        try {
            UserLocationRenderer userLocation = new UserLocationRenderer();
            userLocation.createOnGlThread(this);
            worldRenderer.add(userLocation);

            DestinationRenderer destination = new DestinationRenderer();
            destination.createOnGlThread(this);
            worldRenderer.add(destination);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read obj file");
        }
        try {
            PlaneRenderer plane = new PlaneRenderer();
            plane.createOnGlThread(this);
            worldRenderer.add(plane);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read plane texture");
        }

        try {

            POIRenderer poi = new POIRenderer(mSession, sensorHelper);
            poi.createOnGlThread(this);
            worldRenderer.add(poi);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read plane texture");
        }
        PointCloudRenderer pointCloud = new PointCloudRenderer();
        pointCloud.createOnGlThread(this);
        worldRenderer.add(pointCloud);

        LineRenderer line = new LineRenderer();
        line.createOnGlThread(this);
        worldRenderer.add(line);

        try {
            ArrowRenderer arrow = new ArrowRenderer();
            arrow.createOnGlThread(this);
            worldRenderer.add(arrow);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read arrow model");
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        mDisplayRotationHelper.onSurfaceChanged(width, height);
        GLES20.glViewport(0, 0, width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        // Clear screen to notify driver it should not load any pixels from previous frame.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        if (mSession == null) {
            return;
        }
        // Notify ARCore session that the view size changed so that the perspective matrix and
        // the video background can be properly adjusted.
        mDisplayRotationHelper.updateSessionIfNeeded(mSession);

        try {
            // Find the current AR state
            WorldState worldState = new WorldState(mSession);

            navigator = Navigator.getInstance(this);
            Optional<LatLng> currentPos = navigator.getCurrentPosition();

            //Get first and second destination
            LatLng currDestination = navigator.getPointOnPath(0);
            LatLng nextDestination = navigator.getPointOnPath(1);

            worldState.setIsCurrentFinal(currDestination == navigator.getDestination());

            if (currDestination == null) {
                Log.d("Destination", "null");
                if (navigator.getPath() == null)
                Log.d("End Dest","null");
                else
                    Log.d("End Dest", navigator.getPath().get(navigator.getPath().size()).toString());
            }
            else
                Log.d("Destination", currDestination.toString());
            // Check if we detected at least one plane. If so, hide the loading message.
            if (mMessageSnackbar != null) {
                if (!isSurfaceDetected()) {
                    setLoadingMessage("Waiting for surface...");
                } else if (!currentPos.isPresent()) {
                    setLoadingMessage("Waiting for location...");
                } else if(currDestination == null) {
                    setLoadingMessage("Waiting for path to calculate...");  //wait for path or just have user go back to mainactivity?
                }/*else if (navigator.checkGoal()) {
                    setLoadingMessage("You're here");
                }*/else {
                    hideLoadingMessage();
                }
            }
            // Get the floor!
            Plane floor = worldState.getFloorPlane();
            if (floor != null && currentPos.isPresent() && currDestination != null) {
                // Make the cheating FABs available
                if (!fabsVisible) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            findViewById(R.id.refreshFAB).setVisibility(View.VISIBLE);
                            findViewById(R.id.consumeFAB).setVisibility(View.VISIBLE);
                            fabsVisible = true;
                        }
                    });
                }

                // Find information on the next point on the path relative to the user
                float currDistance = (float) Navigator.distanceBetweenInMeters(
                        currentPos.get(),
                        currDestination);
                float currAngle = ARUtils.getBearing(
                        currentPos.get(),
                        currDestination,
                        sensorHelper.getHeading());
                worldState.setCurrDestinationBearing(currAngle);
                Anchor currAnchor = mSession.createAnchor(worldState.calculatePose(currDistance, currAngle));
                Anchor nextAnchor = null;
                if(!worldState.getIsCurrentFinal()) {
                    float nextDistance = (float) Navigator.distanceBetweenInMeters(
                            currentPos.get(),
                            nextDestination);
                    float nextAngle = ARUtils.getBearing(
                            currentPos.get(),
                            nextDestination,
                            sensorHelper.getHeading());
                    nextAnchor = mSession.createAnchor(worldState.calculatePose(nextDistance, nextAngle));
                }
                if(destAnchor == null || pastCurr == null) {
                    Log.i(TAG, "Created Destination Anchor");
                    destAnchor = new DestinationAnchor(currAnchor,nextAnchor);
                } else if(pastNext == currDestination) {
                    destAnchor.nextDestination(nextAnchor);
                } else if(pastCurr != currDestination){
                    Log.i(TAG,"Off-Path, recalculating");
                    destAnchor = new DestinationAnchor(currAnchor,nextAnchor);
                } else if (nextAnchor == null) {
                    destAnchor.update(mSession, currAnchor.getPose(), null);
                }
                else {
                    destAnchor.update(mSession, currAnchor.getPose(), nextAnchor.getPose());
                }

                // Consume the path node if we're close to it in ARCore terms
                double arcoreDistance = destAnchor.distanceToCurrent(worldState.getCameraPose());
                if (arcoreDistance < WAYPOINT_THRESHOLD) {
                    Log.w(TAG, "Consumed waypoint because of AR");
                    navigator.toNextPointOnPath();
                }

                // Update past destination and past next destination
                pastCurr = currDestination;
                pastNext = nextDestination;


                //Setup for POI
                if (navigator.getTourMode()){
                    if (navigator.getPointsOfInterestNearMe().size() > 0) {
                        //code to draw anchor(s) to screen if its within range
                        Log.i("CameraActivity:Tour", "tour mode on");
                    }
                }

                worldState.setCurrDestinationPose(destAnchor.getCurrAnchor().getPose());
                if (nextAnchor != null) {
                    worldState.setNextDestinationPose(destAnchor.getNextAnchor().getPose());
                }
                else {
                    worldState.setNextDestinationPose(null);
                }
            }

            worldRenderer.drawFrame(this, worldState);

        } catch (Throwable t) {
            // Avoid crashing the application due to unhandled exceptions.
            Log.e(TAG, "Exception on the OpenGL thread", t);
        }
    }

    /**
     * Attempt to initialize the ARCore session. This method should be called until it returns true.
     *
     * @return true if initialization has finished, false if it's not finished yet
     */
    private boolean initARSession() {
        ArCoreApk arCoreApk = ArCoreApk.getInstance();

        Exception exception = null;
        String errorMessage = null;

        try {
            ArCoreApk.InstallStatus installStatus =
                    arCoreApk.requestInstall(this, requestToInstallArCore);

            if (installStatus == ArCoreApk.InstallStatus.INSTALLED) {
                // The user has ARCore
                Log.i(TAG, "Successfully detected ARCore");
            } else if (installStatus == ArCoreApk.InstallStatus.INSTALL_REQUESTED) {
                // The user doesn't yet have ARCore. We've asked them to install, but if we have
                // to ask again, we need to assume that the user chose not to install it
                requestToInstallArCore = false;
                Log.i(TAG, "Asking the user to install ARCore");
                // We're not finished yet
                return false;
            }

            // Initialize ARCore
            mSession = new Session(this);
            Config config = new Config(mSession);
            if (!mSession.isSupported(config)) {
                throw new Exception("The default configuration is not supported");
            }
            mSession.configure(config);
        } catch (UnavailableArcoreNotInstalledException |
                UnavailableUserDeclinedInstallationException e) {
            // The user chose to not install ARCore
            errorMessage = "ARCore is required for AR navigation";
            exception = e;
        } catch (UnavailableApkTooOldException e) {
            // For some reason, ARCore doesn't want to update itself
            errorMessage = "Please update ARCore";
            exception = e;
        } catch (UnavailableSdkTooOldException e) {
            // Our app is too old for the user's ARCore
            errorMessage = "This app needs to be updated for your device";
            exception = e;
        } catch (Exception e) {
            errorMessage = "Your device does not support AR";
            exception = e;
        }

        if (errorMessage != null) {
            // Something went wrong
            showSnackbarMessage(errorMessage, true);
            Log.e(TAG, "Exception creating session", exception);
            return false;
        }

        return true;
    }

    /**
     * @return true if we've found at least one horizontal surface
     */
    private boolean isSurfaceDetected() {
        for (Plane plane : mSession.getAllTrackables(Plane.class)) {
            if (plane.getType() == com.google.ar.core.Plane.Type.HORIZONTAL_UPWARD_FACING
                    && plane.getTrackingState() == TrackingState.TRACKING) {
                return true;
            }
        }

        return false;
    }


    private void showSnackbarMessage(String message, boolean finishOnDismiss) {
        mMessageSnackbar = Snackbar.make(
                CameraActivity.this.findViewById(android.R.id.content),
                message, Snackbar.LENGTH_INDEFINITE);
        mMessageSnackbar.getView().setBackgroundColor(0xbf323232);
        if (finishOnDismiss) {
            mMessageSnackbar.setAction(
                    "Dismiss",
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMessageSnackbar.dismiss();
                        }
                    });
            mMessageSnackbar.addCallback(
                    new BaseTransientBottomBar.BaseCallback<Snackbar>() {
                        @Override
                        public void onDismissed(Snackbar transientBottomBar, int event) {
                            super.onDismissed(transientBottomBar, event);
                            finish();
                        }
                    });
        }
        mMessageSnackbar.show();
    }

    private void showLoadingMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showSnackbarMessage("", false);
            }
        });
    }

    private void setLoadingMessage(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessageSnackbar.setText(message);
            }
        });
    }

    private void hideLoadingMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mMessageSnackbar != null) {
                    mMessageSnackbar.dismiss();
                }
                mMessageSnackbar = null;
            }
        });
    }

}
