package com.asu.navigatear.rendering;

import android.content.Context;

import com.asu.navigatear.WorldState;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains a list of renderables and manages how they are drawn every frame.
 */
public class WorldRenderer {

    private List<Renderable> renderables;

    /**
     * Create a new WorldRenderer.
     */
    public WorldRenderer() {
        renderables = new ArrayList<>();
    }

    /**
     * Adds a new renderable to be rendered in future frames.
     * @param renderable the renderable to render
     */
    public void add(Renderable renderable) {
        renderables.add(renderable);
    }

    /**
     * Draws a single frame given the current AR state.
     * @param ctx the Android context this draw command was run in
     * @param worldState the current AR state
     */
    public void drawFrame(Context ctx, WorldState worldState) {
        for (Renderable renderable : renderables) {
            renderable.draw(ctx, worldState);
        }
    }
}
