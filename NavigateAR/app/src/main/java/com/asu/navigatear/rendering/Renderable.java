package com.asu.navigatear.rendering;

import android.content.Context;

import com.asu.navigatear.WorldState;

/**
 * Describes an object that can be rendered on-screen and requires AR information to do so.
 */
public interface Renderable {
    void draw(Context ctx, WorldState worldState);
}
