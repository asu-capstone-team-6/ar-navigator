package com.asu.navigatear;

import android.util.Log;

import com.google.ar.core.Camera;
import com.google.ar.core.Frame;
import com.google.ar.core.Plane;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides access to information on the current state of the AR world. These objects are stale
 * past the frame they are created in.
 */
public class WorldState {
    private static final String TAG = WorldState.class.getSimpleName();

    private Session session;
    private Frame frame;
    private Camera camera;
    private Pose currDestinationPose;
    private Pose nextDestinationPose;
    private boolean isCurrentFinal;
    private float currDestinationBearing;

    /**
     * Returns a WorldState with information the current frame.
     * @param session the active ARCore session
     */
    public WorldState(Session session) {
        this.session = session;
        this.frame = session.update();
        this.camera = frame.getCamera();
    }

    /**
     * Returns the frame that this WorldState is in reference to.
     * @return current frame
     */
    public Frame getFrame() {
        return this.frame;
    }

    /**
     * Returns this frame's projection matrix.
     * @return projection matrix
     */
    public float[] getProjectionMatrix() {
        // Get projection matrix.
        float[] projmtx = new float[16];
        camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);
        return projmtx;
    }

    /**
     * Returns this frame's view matrix.
     * @return view matrix
     */
    public float[] getCameraViewMatrix() {
        // Get camera matrix and draw.
        float[] viewmtx = new float[16];
        camera.getViewMatrix(viewmtx, 0);
        return viewmtx;
    }

    /**
     * Returns a float estimating how intense the light is in the frame. Used to match the lighting
     * of AR objects with the surrounding lighting.
     * @return light intensity
     */
    public float getLightIntensity() {
        return frame.getLightEstimate().getPixelIntensity();
    }

    /**
     * Returns all trackable frames in the AR world at the current frame.
     * @return list of planes
     */
    public Collection<Plane> getAllPlanes() {
        return session.getAllTrackables(Plane.class);
    }

    /**
     * Finds the plane that is most likely the ground of those that are currently being tracked.
     * @return plane that is likely the floor
     */
    public Plane getFloorPlane() {
        List<Plane> planes = new ArrayList<>();
        List<Float> areas = new ArrayList<>();

        // Get all tracked planes and their areas
        for (Trackable trackable : session.getAllTrackables(Plane.class)) {
            if (trackable.getTrackingState() == TrackingState.TRACKING) {
                Plane plane = (Plane) trackable;
                planes.add(plane);
                areas.add(plane.getExtentX() * plane.getExtentZ());
            }
        }

        // Take care of vacuous cases
        if (planes.size() == 0) {
            return null;
        } else if (planes.size() == 1) {
            return planes.get(0);
        }

        // If the largest plane is a standard deviation away from the average, assume it's the floor
        float stddev = MathUtils.stddev(areas);
        int maxIndex = MathUtils.maxIndex(areas);
        if (areas.get(maxIndex) >= stddev) {
            return planes.get(maxIndex);
        }

        // Fall back to using the lowest plane if no obvious largest plane exists
        int lowestIndex = 0;
        for (int i=0; i<planes.size(); i++) {
            if (planes.get(i).getCenterPose().ty() < planes.get(lowestIndex).getCenterPose().ty()) {
                lowestIndex = i;
            }
        }
        return planes.get(lowestIndex);
    }

    /**
     * Returns the pose of the user's feet. More precisely, it's the pose on the floor right under
     * the device.
     * @return pose of feet
     */
    public Pose getFootPose() {
        Pose camPose = camera.getPose();
        Pose floorPose = getFloorPlane().getCenterPose();
        return ARUtils.getOffsetPose(camPose, floorPose, 0, 0);
    }

    /**
     * Creates a pose a distance away from the camera at a given angle.
     * @param drawDistance how far away the pose will be from the camera
     * @param angle the angle from the camera to put the pose
     * @return pose far away from the camera
     */
    public Pose calculatePose(float drawDistance, float angle) {
        Pose camPose = camera.getPose();
        Pose floorPose = getFloorPlane().getCenterPose();
        return ARUtils.getOffsetPose(camPose, floorPose, drawDistance, angle);
    }

    public Pose getCameraPose() {
        return camera.getDisplayOrientedPose();
    }

    public void setCurrDestinationPose(Pose pose) {
        this.currDestinationPose = pose;
    }
    public void setNextDestinationPose(Pose pose) {
        this.nextDestinationPose = pose;
    }

    public Pose getCurrDestinationPose() {
        return currDestinationPose;
    }

    public Pose getNextDestinationPose() {
        return nextDestinationPose;
    }

    /**
     * Sets whether or not the current destination is the last one.
     * @param isCurrentFinal value to set
     */
    public void setIsCurrentFinal(boolean isCurrentFinal) {
        this.isCurrentFinal = isCurrentFinal;
    }

    /**
     * @return true if the current destination is the last one
     */
    public boolean getIsCurrentFinal() {
        return isCurrentFinal;
    }

    /**
     * Returns the user's bearing relative to the current destination.
     * @return bearing
     */
    public float getCurrDestinationBearing() {
        return currDestinationBearing;
    }

    /**
     * Sets the user's bearing relative to the current destination.
     * @param bearing bearing
     */
    public void setCurrDestinationBearing(float bearing) {
        this.currDestinationBearing = bearing;
    }
}

