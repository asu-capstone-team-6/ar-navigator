package com.asu.navigatear;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A point of interest on campus. Some kind of interesting or iconic feature.
 */
class PointOfInterest {

    private String name;
    private String content;
    private String imageName;
    private LatLng location;

    /**
     * Creates a new point of interest given a JSONObject.
     * @param json the JSON object to extract info from
     * @throws JSONException thrown if the JSON object is malformed
     */
    PointOfInterest(JSONObject json) throws JSONException {
        this.name = json.getString("name");
        if(json.has("content")){
            this.content = json.getString("content");
        }
        else{
            this.content = "";
        }
        if(json.has("image name")){
            this.imageName = json.getString("image name");
        }
        else{
            this.imageName = "fountain";
        }

        this.location = new LatLng(json.getDouble("lat"),json.getDouble("lng"));
    }

    String getName() {
        return name;
    }

    String getContent() {
        return content;
    }

    String getImageName() {
        return imageName;
    }

    LatLng getLocation() {
        return location;
    }
}
