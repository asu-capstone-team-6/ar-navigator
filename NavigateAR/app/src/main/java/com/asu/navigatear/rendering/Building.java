package com.asu.navigatear.rendering;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rahuldawar on 2/5/18.
 */

public class Building {

    public String name;
    public Double lat, lng;

    Building(JSONObject jo, String nm){
        try {
            name = nm;
            lat = jo.getDouble("lat");
            lng = jo.getDouble("lng");
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

}
