package com.asu.navigatear;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Hashtable;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Polyline path_line;
    private Polyline tour_line;
    private ArrayList<Marker> poi_markers;
    private boolean poi_marked;
    private boolean initialZoom;
    private Marker currentPositionMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Toolbar tbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(tbar);

        // Get a support ActionBar corresponding to this toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.gmap);
        mapFragment.getMapAsync(this);
    }

    public void arView(View view){
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }
    private Polyline plotPath(ArrayList<LatLng> locs,int color){
        PolylineOptions opts = new PolylineOptions()
                .jointType(JointType.ROUND)
                .width(10)
                .color(color);
        for(int i = 0;i<locs.size();i++){
            opts.add(locs.get(i));
        }
        return mMap.addPolyline(opts);
    }
    private void renderPathAndPOI(LatLng position){

        Navigator navi = Navigator.getInstance(this);
        if(navi.getPath() != null && navi.getPath().size() >= 2) {
            if(path_line != null){
                path_line.remove();
            }
            path_line = plotPath(navi.getPath(), Color.BLACK);
        }

        //enable poi markers
        if(!poi_marked) {
            poi_markers = new ArrayList<Marker>();
            for (PointOfInterest poi : navi.getPointsOfInterest()) {
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(poi.getLocation())
                        .title(poi.getName()));
                poi_markers.add(marker);
            }
            final Context c = this;
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                public boolean onMarkerClick(Marker marker) {
                    Intent intent = new Intent(c, PoiActivity.class);
                    intent.putExtra("point of interest name", marker.getTitle());
                    startActivity(intent);
                    return true;
                }
            });
            poi_marked = true;
        }

        //zoom
        if(!initialZoom && navi.getPath() != null && navi.getPath().size() >= 1) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for(LatLng l: navi.getPath()){
                builder.include(l);
            }
            CameraUpdate camPos = CameraUpdateFactory.newLatLngBounds(builder.build(),50);
            mMap.animateCamera(camPos);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try{
            mMap = googleMap;
        }
        catch(Exception e){}

        poi_marked = false;
        initialZoom = false;
        Navigator nav = Navigator.getInstance(this);

        if(nav.getPath() != null)
            renderPathAndPOI(nav.getCurrentPosition().get());

        nav.subscribeToLocationUpdates(new LocationUpdateListener(){
            @Override
            public void onLocationChanged(LatLng position) {
                renderPathAndPOI(position);
            }
        });
    }

    @Override
    public void onBackPressed(){
        Intent intent =  new Intent(MapsActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
