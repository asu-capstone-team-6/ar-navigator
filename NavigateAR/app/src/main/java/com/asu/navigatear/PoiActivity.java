package com.asu.navigatear;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Shows a picture and information on a point of interest.
 */
public class PoiActivity extends AppCompatActivity {
    private PointOfInterest poi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi);

        // Setup the toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Grab info on the POI we're displaying
        String poiName = getIntent().getStringExtra("point of interest name");
        if (poiName != null) {
            Navigator navigator = Navigator.getInstance(this);

            poi = navigator.getPointOfInterest(poiName);
            if (poi != null) {
                TextView content = findViewById(R.id.poi_info);
                TextView title = findViewById(R.id.poi_title);
                ImageView image = findViewById(R.id.poi_view);

                // Update the content shown on screen to match the POI data
                content.setText(poi.getContent());
                title.setText(poi.getName());
                int imageId = getResources().getIdentifier(
                        poi.getImageName(),
                        "drawable",
                        getPackageName());
                image.setImageResource(imageId);
            }
        }
    }

    /**
     * Called when the FAB in the activity is pressed, indicating that the user wants to go to that
     * POI.
     */
    public void navigateToPoi(View v) {
        if(poi != null){
            Navigator nav = Navigator.getInstance(this);
            nav.setDestination(poi.getLocation());
            Intent intent = new Intent(this, MapsActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Add an overflow menu with options
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            // Go to the settings activity
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
