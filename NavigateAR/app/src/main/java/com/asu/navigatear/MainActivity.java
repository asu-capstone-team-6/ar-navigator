package com.asu.navigatear;


import android.annotation.SuppressLint;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.support.v7.widget.SearchView;
import android.view.inputmethod.InputMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static android.widget.Toast.LENGTH_SHORT;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();




    private boolean spinnerInitalized = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Build the spinner immediately if we already have permissions. If not, it will be
        // initialized once permissions have been accepted

    }

    @Override
    public void onBackPressed(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.addFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Ask for the required permissions
        if (!Permissions.hasPermissions(this)) {
            // Runs onRequestPermissionsResult when finished
            Permissions.requestMissingPermissions(this);
        }else{
            updateSpinner();
        }
    }


    public void explore(View view){
        //Intent intent = new Intent(this, MapsActivity.class);
        //Enabling AR Tour capabilities
        //This should create the tour path based on navigator
        Intent intent = new Intent(this, CameraActivity.class);
        Navigator nav = Navigator.getInstance(this);
        nav.enableTour();
        startActivity(intent);
    }

    public void settings(View view){
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    /**
     * Called when the user has answered our permission request.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != Permissions.CODE) {
            // Some other permission request we didn't ask for
            return;
        }

        // Check if we've been given the required permissions
        for (int i=0; i<permissions.length; i++) {
            if (grantResults[i] == PERMISSION_DENIED) {
                Toast.makeText(this,
                        "Camera and location permissions are required for NavigateAR",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }
        updateSpinner();
    }


    @SuppressLint("WrongViewCast")
    void updateSpinner(){
        final Navigator navigator = Navigator.getInstance(this);
        List<String> buildingNames = navigator.getBuildingFullNames();
        buildingNames.add(0, "Where would you like to go?");
        Spinner sp1 = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> places = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, buildingNames);

        places.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(places);
        final ArrayAdapter<String> PLACESF = places;
        spinnerInitalized = true;
        final MainActivity thisAct = this;
        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if(position != 0) {
                    String place = PLACESF.getItem(position);
                    String destCode = navigator.getBuildingCode(place);
                    Intent ar = new Intent(thisAct, CameraActivity.class);
                    if (navigator.setDestination(destCode)) {
                        startActivity(ar);
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
}
