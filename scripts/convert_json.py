import json
from PIL import Image
import pyautogui as pg

with open('NavigateAR/app/src/main/res/raw/vertices.json') as vertjs:

    verts = json.load(vertjs)['points']

vertices = {}
for i in range(0,len(verts)):
    vertices[str(i)] = {}
    vertices[str(i)]['lat'] = float(verts[i]['lat'])
    vertices[str(i)]['lng'] = float(verts[i]['lng'])
    vertices[str(i)]['edges'] = []


with open('vertices.json','w+') as verticesjs:
    json.dump(vertices,verticesjs,indent=4,sort_keys=True)



tl_c = vertices['0']
tr_c = vertices['0']
bl_c = vertices['0']
br_c = vertices['0']


min_lat = vertices['0']['lat']
min_lng = vertices['0']['lng']
max_lat = vertices['0']['lat']
max_lng = vertices['0']['lng']

'''
"sdfc":{"lat":33.307782,"lng":-111.675223},
"tech":{"lat":33.305408,"lng":-111.680153},
'''

for vrt in vertices:
    v = vertices[vrt]
    min_lat = min([min_lat,v['lat']])
    min_lng = min([min_lng,v['lng']])
    max_lat = max([max_lat,v['lat']])
    max_lng = max([max_lng,v['lng']])

    if v['lat'] > tl_c['lat'] and v['lng'] > tl_c['lng']: #top left
        tl_c = v
    if v['lat'] > tr_c['lat'] and v['lng'] < tr_c['lng']: #top right
        tr_c = v
    if v['lat'] < bl_c['lat'] and v['lng'] > bl_c['lng']: #bottom left
        bl_c = v
    if v['lat'] < br_c['lat'] and v['lng'] < br_c['lng']: #bottom right
        br_c = v

print('top left: ' + str(tl_c))
print('top right: ' + str(tr_c))
print('bottom left: ' + str(bl_c))
print('bottom right: ' + str(br_c))
print('top left align: (lat: ' + str(max_lat) + ', lng: ' + str(max_lng)+')')
print('bottom right align: (lat: ' + str(min_lat) + ', lng: ' + str(min_lng)+')')
SIZE = 1000
dots = {}
SCALAR = max([max_lng-min_lng,max_lat-min_lat])
SCALAR = int(SIZE/SCALAR)
for vrt in vertices:
    v = vertices[vrt]
    dots[vrt] = {'x':int((v['lng']-min_lng)*SCALAR),'y':int((v['lat']-min_lat)*SCALAR)}

#print(dots)


img = Image.new('RGBA', (SIZE,SIZE),color=None)
for dt in dots:
    d = dots[dt]
    img.putpixel((d['x'],d['y']),(255,0,0,255))
#img.show()
img = img.rotate(180)
img.save('dot_overylay.png')
