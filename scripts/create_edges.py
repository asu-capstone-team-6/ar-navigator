import json
import math
DISTANCE_THRESHOLD = .0001


def dist(p1,p2):
    return math.sqrt((abs(p1[0]-p2[0]))**2 + (abs(p1[1]-p2[1]))**2)

def toTup(arr):
    return (arr[0],arr[1])

segs = []
with open('map.geojson') as ljs:
    lines = json.load(ljs)

lines = lines['features']
for ln1 in lines:
    ln = ln1['geometry']['coordinates']
    for i in range(0,len(ln) -1):
        segs.append([ln[i],ln[i+1]])

#print(len(segs))



unique_points_list = []

for seg in segs:
    unique1 = True
    unique2 = True
    for pt in unique_points_list:
        if dist(seg[0],pt) < DISTANCE_THRESHOLD:
            unique1 = False
            seg[0] = pt
        if dist(seg[1],pt) < DISTANCE_THRESHOLD:
            unique2 = False
            seg[1] = pt
    if unique1:
        unique_points_list.append(seg[0])
    if unique2:
        unique_points_list.append(seg[1])

final_points = {}

for i in range(0,len(unique_points_list)):
    pt = unique_points_list[i]
    xpt = {'lat':pt[1],'lng':pt[0],'edges':[]}
    for seg in segs:
        if seg[0][0] == pt[0] and seg[0][1] == pt[1]:
            s_ind = str(unique_points_list.index(seg[1]))
            if s_ind not in xpt['edges']:
                xpt['edges'].append(s_ind)
        if seg[1][0] == pt[0] and seg[1][1] == pt[1]:
            s_ind = str(unique_points_list.index(seg[0]))
            if s_ind not in xpt['edges']:
                xpt['edges'].append(s_ind)
    final_points[str(i)] = xpt

with open('edges.json','w+') as ejs:
    json.dump(final_points,ejs,indent=4,sort_keys=True)
